'use strict';

/**
 * @ngdoc function
 * @name asodecApp.controller:JuntaDirectivaCtrl
 * @description
 * # JuntaDirectivaCtrl
 * Controller of the asodecApp
 */
angular.module('asodecApp')
  .controller('JuntaDirectivaCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
