'use strict';

/**
 * @ngdoc function
 * @name asodecApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the asodecApp
 */
angular.module('asodecApp')
.controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
});