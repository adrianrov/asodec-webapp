'use strict';

/**
 * @ngdoc function
 * @name asodecApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the asodecApp
 */
angular.module('asodecApp')
  .controller('ContactCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
