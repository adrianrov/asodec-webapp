'use strict';

/**
 * @ngdoc function
 * @name asodecApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the asodecApp
 */
angular.module('asodecApp')
.controller('AboutCtrl', function ($scope) {
	$scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
});
