'use strict';

/**
 * @ngdoc function
 * @name asodecApp.controller:SemanaCompuCtrl
 * @description
 * # SemanaCompuCtrl
 * Controller of the asodecApp
 */
angular.module('asodecApp')
  .controller('SemanaCompuCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
