'use strict';

/**
 * @ngdoc directive
 * @name asodecApp.directive:buttonCollapse
 * @description
 * # buttonCollapse
 */
angular.module('asodecApp')
  .directive('buttonCollapse', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element) {
        element.sideNav();
      }
    };
  });
