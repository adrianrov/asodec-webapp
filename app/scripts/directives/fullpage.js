'use strict';

/**
 * @ngdoc directive
 * @name asodecApp.directive:fullpage
 * @description
 * # fullpage
 */
angular.module('asodecApp')
  .directive('fullpage', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element) {
        element.fullpage({
        	autoScrolling: false,
        	normalScrollElements: '.container'
        });
      }
    };
  });
