'use strict';

/**
 * @ngdoc directive
 * @name asodecApp.directive:parallax
 * @description
 * # parallax
 */
angular.module('asodecApp')
  .directive('parallax', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element) {
        element.parallax();
      }
    };
  });
