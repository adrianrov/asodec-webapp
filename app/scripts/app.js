'use strict';

/**
 * @ngdoc overview
 * @name asodecApp
 * @description
 * # asodecApp
 *
 * Main module of the application.
 */
angular
  .module('asodecApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/contactenos', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
    .when('/tienda', {
        templateUrl: 'views/tienda.html',
        controller: 'StoreCtrl'
      })
      .when('/semana-compu', {
        templateUrl: 'views/semana-compu.html',
        controller: 'SemanaCompuCtrl'
      })
      .when('/junta-directiva', {
        templateUrl: 'views/junta-directiva.html',
        controller: 'JuntaDirectivaCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });