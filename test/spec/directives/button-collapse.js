'use strict';

describe('Directive: buttonCollapse', function () {

  // load the directive's module
  beforeEach(module('asodecApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<button-collapse></button-collapse>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the buttonCollapse directive');
  }));
});
