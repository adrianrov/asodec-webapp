'use strict';

describe('Directive: fullpage', function () {

  // load the directive's module
  beforeEach(module('asodecWebpageApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<fullpage></fullpage>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the fullpage directive');
  }));
});
