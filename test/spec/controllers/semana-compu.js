'use strict';

describe('Controller: SemanaCompuCtrl', function () {

  // load the controller's module
  beforeEach(module('asodecApp'));

  var SemanaCompuCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SemanaCompuCtrl = $controller('SemanaCompuCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
