'use strict';

describe('Controller: JuntaDirectivaCtrl', function () {

  // load the controller's module
  beforeEach(module('asodecApp'));

  var JuntaDirectivaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    JuntaDirectivaCtrl = $controller('JuntaDirectivaCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
